---
pagetitle: EN
---

# Zhou Siwei

- [cv](https://zhousiwei.gitee.io/cv/)
- [中文简历](./resume.html)

## Java Software Engineer

- phone: +86 13647910412
- email: [2434387555@qq.com](https://zhousiwei.gitee.io)
- Personal blog: [https://zhousiwei.gitee.io](https://zhousiwei.gitee.io)
- GitHub: [https://github.com/JoeyBling](https://github.com/JoeyBling)
- Notes: [https://zhousiwei.gitee.io/ibooks](https://zhousiwei.gitee.io/ibooks)

## Self-introduction

&#160;&#160;&#160;&#160;I am a Java developer who loves **open source**，Like to share their own code on open source sites，Make it easy for others to learn and provide Suggestions to improve the code。I pursue the development of ** high quality ** and ** good user experience ** application，I focus on ** development efficiency ** and ** knowledge accumulation **。<br/>
&#160;&#160;&#160;&#160;Familiar with the whole process of software development, with strong logical thinking ability. Good documentation and code writing skills, able to solve problems independently, persistent and detail-oriented.<br/>
&#160;&#160;&#160;&#160;Major Technology：**Java、Third Party Pay、Wechat_Mp、Open Source Enthusiasts、Linux**

> Not impetuous, not radical, not conservative, love programming, love the back end, love new technology, love sports, love travel, execution and learning ability are awesome!

## Work experience

### **Java Software Engineer** [Hangzhou TEYANG network technology co., LTD](http://www.hztywl.cn/)

***2019/4 - 至今***

* Participated in the construction of aggregation payment framework (alipay, WeChat, unionpay), and the implementation of IAP internal purchase payment
* Using high concurrency, multithreading provides solutions for (Internet + healthcare)
* Responsible for technical specification document related to product or project, improve team development efficiency
* Use Linux command to complete daily system deployment and maintenance work

### **Java Software Engineer** [Jiangxi 3xData information technology co. LTD](http://www.3xdata.cn/)

***2017/12 - 2019/03***

* Using SSM, SpringCloud, Nacos, VueJS and other cutting-edge development technologies
* Build ELK log analysis platform to analyze and display massive log data
* Excellent employee certificate awarded and sealed by the company
* Interface with front-end developers to solve bugs raised by testers

### **Java Software Engineer** [Jiangxi yiqiang weiying information technology co., LTD](http://jxyq123.51sole.com/)

***2016/06 - 2017/11***

* Participate in the discussion of relevant project requirements, outline design, and complete the core code writing, improve the team development efficiency
* Development with SpringBoot, SpringMVC and Mybatis
* Continuous code reconstruction and optimization, analysis of system deficiencies, improve system performance
* Encapsulate common code base, make it more standardized, reduce team workload and improve development efficiency

## Technology and language

- **Java**: `SpringBoot`、`SpringCloud`、`SpringMVC`、`MyBatis`、`Shiro`、`Freemarker`
- **Front end**: `VueJs`、`Bootstrap`、`LayUI`、`jQuery UI`
- **Database**: `MySQL/MariaDB`、`SQLServer`、`Oracle`、`MongoDB`、`redis`、`memcached`
- **Web server**: `Nginx`、`Tomcat`、`Apache`、`Jetty`
- **OS**: `Linux`、`Windows`
- **Software**: `Git`、`Svn`、`Maven`、`IDEA`、`Postman`、`XMind`、`Visio`、`NodeJS`、`Photoshop`

## Opensource Project

1. [A privilege management framework based on SpringBoot + Shiro + MyBatisPlus](https://gitee.com/zhousiwei/bootplus)
2. [SpringBoot integration with MyBatisPlus integration with Shiro allows rapid development](https://gitee.com/zhousiwei/springboot_mybatisplus)
3. [Spring boot starter for fastjson](https://gitee.com/zhousiwei/fastjson-spring-boot-starter)
4. [Web kettle developed a set of distributed integrated scheduling, management,ETL development user professional B/S architecture tools](https://github.com/JoeyBling/webkettle)
5. [A dynamic resume](https://gitee.com/zhousiwei/anires)
6. [Record technical development notes with VuePress](https://gitee.com/zhousiwei/ibooks)
7. [Use GitBook to record development notes](https://zhousiwei.gitee.io/myBook/)
8. [My blog site](https://gitee.com/zhousiwei/zhousiwei)

## Education experience

*2014 - 2017* [Northwestern polytechnic university](http://www.nwpu.edu.cn/) Computer information major B.S.

## Interests and hobbies

Love toss, like to explore and try not to do new things, amateur love playing table tennis, running.

## Zhousiwei's notes：[https://zhousiwei.gitee.io/ibooks/](https://zhousiwei.gitee.io/ibooks/)

1. [Opensource projects](https://zhousiwei.gitee.io/ibooks/)
2. [Java](https://zhousiwei.gitee.io/ibooks/java/springboot2.html)
3. [Front end](https://zhousiwei.gitee.io/ibooks/web/)
4. [Hexo](https://zhousiwei.gitee.io/ibooks/hexo/hexo_music.html)
5. [Notes](https://zhousiwei.gitee.io/ibooks/notes/git_branch.html)
6. [Linux](https://zhousiwei.gitee.io/ibooks/linux/glibc.html)
7. [ELK](https://zhousiwei.gitee.io/ibooks/elk/linux_es5.html)

## Contact me
* QQ：**2434387555** | WeChat：**13647910412**

> If you like this effect，Fork [My project](https://gitee.com/zhousiwei/iresume)，Create your own resume！
