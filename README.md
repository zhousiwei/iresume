## [Java软件工程师简历](http://zhousiwei.gitee.io/iresume/resume.html)

> **欢迎使用和Star支持，如使用过程中碰到问题，可以提出Issue，我会尽力完善**

## 介绍
&#160;&#160;&#160;&#160;`Markdown`格式的简历最适合不过我们技术人员了，再加上强大的格式转换工具： **[Pandoc](https://pandoc.org)**，我们只需要一份`Markdown`格式的简历便可轻轻松松管理所有格式的简历了，比如下方的`HTML`格式的简历，就是来源于此，如果觉得我这份模板样子不错，小伙伴们可以fork此项目后自己修改其个人信息。

## 线上预览
* [我的中文简历-Markdown](./resume.md)

* [我的中文简历-HTML](http://zhousiwei.gitee.io/iresume/resume.html)

* [我的英文简历-Markdown](./resume-en.md)

* [我的英文简历-HTML](http://zhousiwei.gitee.io/iresume/resume-en.html)

## 项目截图

[![試毅-思伟的个人简历](./screenshots/1_mini.png "試毅-思伟的个人简历")](http://zhousiwei.gitee.io/iresume/resume.html "試毅-思伟的个人简历")

## 本地预览
1. 安装[Pandoc](https://pandoc.org/installing.html)

2. 修改脚本中`--title-prefix`参数，后面html显示的title内容

3. 修改`Markdown`文件中的个人信息

4. Linux环境下运行`to_html.sh` | Windows环境运行`to_html.bat`

5. 用浏览器查看生成后的html文件

# 关于我
- [个人博客](https://zhousiwei.gitee.io/)
- [技术笔记](https://zhousiwei.gitee.io/ibooks/)
- [GitHub](https://github.com/JoeyBling)
- [码云](https://gitee.com/zhousiwei)
- [简书](https://www.jianshu.com/u/02cbf31a043a)
- [CSDN](https://blog.csdn.net/qq_30930805)
- [知乎](https://www.zhihu.com/people/joeybling)
- [微博](http://weibo.com/jayinfo)
- **主要涉及技术：`Java后端开发`、`聚合支付`、`公众号开发`、`开源爱好者`、`Linux`**

## License

[Apache License](./LICENSE)