﻿REM 声明采用UTF-8编码
chcp 65001

pandoc -f markdown -t html -o resume-en.html resume-en.md --title-prefix "Zhousiwei's resume" -c css/main.css
pandoc -f markdown -t html -o resume.html resume.md --title-prefix "試毅-思伟的简历" -c css/main.css
pause
