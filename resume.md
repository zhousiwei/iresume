---
pagetitle: 中文
---

# 周思伟

- [中英文简历](https://zhousiwei.gitee.io/cv/)
- [English](./resume-en.html)

## Java软件工程师

- 联系电话: +86 13647910412
- 电子邮箱: [2434387555@qq.com](https://zhousiwei.gitee.io)
- 个人博客: [https://zhousiwei.gitee.io](https://zhousiwei.gitee.io)
- GitHub: [https://github.com/JoeyBling](https://github.com/JoeyBling)
- 技术笔记: [https://zhousiwei.gitee.io/ibooks](https://zhousiwei.gitee.io/ibooks)

## 自我介绍

&#160;&#160;&#160;&#160;我是一名热爱**开源**的Java开发者，喜欢把自己的代码分享在开源网站，方便他人学习和提供建议从而完善代码。我追求开发**高质量**及拥有**良好用户体验**的应用，我注重**开发效率**及**知识的累积**。<br/>
&#160;&#160;&#160;&#160;熟悉软件开发的整个流程，有较强的逻辑思维能力。有良好的文档编写和代码书写规范，能独立解决问题、百折不挠、细节控。<br/>
&#160;&#160;&#160;&#160;主要涉及技术：**Java后端开发、聚合支付、公众号开发、开源爱好者、Linux**

> 不浮躁，不偏激，爱编程，爱后端，爱新技术，爱运动，执行力和学习能力都棒棒哒！

## 工作经验

### **Java软件工程师** [杭州特扬网络科技有限公司](http://www.hztywl.cn/)

***2019/4 - 至今***

* 参与聚合支付框架的搭建（支付宝、微信、银联）、以及IAP内购支付实现
* 使用高并发，多线程为(互联网+医疗)提供解决方案
* 负责编制与产品或项目相关的技术规范文档，提升团队开发效率
* 使用Linux命令完成日常系统的部署及维护工作

### **Java软件工程师** [江西三叉数信息科技有限公司](http://www.3xdata.cn/)

***2017/12 - 2019/03***

* 使用SSM、SpringCloud、Nacos、VueJS等前沿开发技术
* 搭建ELK 日志分析平台，对海量日志数据进行分析展示
* 获得过公司颁发并盖章的优秀员工荣誉证书奖
* 与前端开发人员进行功能接口对接，解决测试人员提出的 bug

### **Java软件工程师** [江西益强微盈信息科技有限公司](http://jxyq123.51sole.com/)

***2016/06 - 2017/11***

* 参加讨论相关项目需求、概要设计，并完成核心代码编写，提升团队开发效率
* 使用SpringBoot、SpringMVC、Mybatis进行开发
* 对代码持续重构优化，对系统不足进行分析，提高系统性能
* 封装公共代码库，使其更为规范统一，减少团队任务量从而提升开发效率

## 技术及语言

- **Java**: `SpringBoot`、`SpringCloud`、`SpringMVC`、`MyBatis`、`Shiro`、`Freemarker`
- **前端**: `VueJs`、`Bootstrap`、`LayUI`、`jQuery UI`
- **数据库**: `MySQL/MariaDB`、`SQLServer`、`Oracle`、`MongoDB`、`redis`、`memcached`
- **web 服务器**: `Nginx`、`Tomcat`、`Apache`、`Jetty`
- **OS**: `Linux`、`Windows`
- **工具软件**: `Git`、`Svn`、`Maven`、`IDEA`、`Postman`、`XMind`、`Visio`、`NodeJS`、`Photoshop`

## 开源项目

1. [基于SpringBoot + Shiro + MyBatisPlus的权限管理框架](https://gitee.com/zhousiwei/bootplus)
2. [SpringBoot集成MyBatisPlus集成Shiro可以快速开发](https://gitee.com/zhousiwei/springboot_mybatisplus)
3. [自制spring boot starter for fastjson](https://gitee.com/zhousiwei/fastjson-spring-boot-starter)
4. [基于web版kettle开发的一套分布式综合调度,管理,ETL开发的用户专业版B/S架构工具](https://github.com/JoeyBling/webkettle)
5. [一个会动的简历](https://gitee.com/zhousiwei/anires)
6. [VuePress记录技术开发笔记](https://gitee.com/zhousiwei/ibooks)
7. [使用GitBook记录开发笔记](https://zhousiwei.gitee.io/myBook/)
8. [試毅-思伟的技术博客网站](https://gitee.com/zhousiwei/zhousiwei)

## 教育经历

*2014 - 2017* [西北工业大学](http://www.nwpu.edu.cn/) 计算机信息专业 本科

## 兴趣爱好

爱折腾，喜欢探索和尝试没有干过的新鲜事物，业余喜爱打乒乓球、跑步。

## 試毅-思伟_开发笔记：[https://zhousiwei.gitee.io/ibooks/](https://zhousiwei.gitee.io/ibooks/)

1. [开源项目介绍](https://zhousiwei.gitee.io/ibooks/)
2. [Java笔记](https://zhousiwei.gitee.io/ibooks/java/springboot2.html)
3. [前端开发笔记](https://zhousiwei.gitee.io/ibooks/web/)
4. [Hexo博客笔记](https://zhousiwei.gitee.io/ibooks/hexo/hexo_music.html)
5. [日记本](https://zhousiwei.gitee.io/ibooks/notes/git_branch.html)
6. [Linux笔记](https://zhousiwei.gitee.io/ibooks/linux/glibc.html)
7. [ELK日志分析](https://zhousiwei.gitee.io/ibooks/elk/linux_es5.html)

## 联系我
* 联系QQ：**2434387555** | 微信：**13647910412**

> 如果你喜欢这个效果，Fork [我的项目](https://gitee.com/zhousiwei/iresume)，打造你自己的简历！
